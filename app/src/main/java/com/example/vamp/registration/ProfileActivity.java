package com.example.vamp.registration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {
    TextView vivod, vivodPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Validator validator = new Validator();
        vivod = (TextView) findViewById(R.id.vivod);
        vivodPassword = (TextView) findViewById(R.id.vivodPassword);
        Intent intent = getIntent();
        String username = intent.getExtras().getString("Login");
        String x = intent.getExtras().getString("Password");
        vivod.setText("Добро пожаловать, " + username);
        vivodPassword.setText("Оценка пароля, " + validator.strongPass(x));
    }
}
