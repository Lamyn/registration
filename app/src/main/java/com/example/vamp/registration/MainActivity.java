package com.example.vamp.registration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView text;
    private Button bRegister;
    private EditText etLogin, etPassword, etRepetition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.korotkiiPassword);
        bRegister = (Button) findViewById(R.id.b_register);
        etLogin = (EditText) findViewById(R.id.et_login);
        etPassword = (EditText) findViewById(R.id.et_password);
        etRepetition = (EditText) findViewById(R.id.et_repetition);
        bRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (etPassword.length() == 0 || etLogin.length() == 0 || etRepetition.length() == 0) {
            text.setText("Какое то поле пустое!");
        } else if (etPassword.getText().length() < 6 && etRepetition.getText().length() < 6) {
            text.setText("Пароль должен содержать больше 6 символов");
        } else if (etPassword.getText().length() < 4) {
            text.setText("Пароль слишком простой!");
        } else if (!(etRepetition.getText().toString().equals(etPassword.getText().toString()))) {
            text.setText("Пароли не совпадают!");
        } else {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra("Login", etLogin.getText().toString());
            intent.putExtra("Password", etPassword.getText().toString());
            startActivity(intent);

        }
    }
}
