package com.example.vamp.registration;

public class Validator {
    int strongPass(String x){
        int strong = 0;
        if (x.length() > 6) strong += 5;
        else { strong += 3; }
        if (x.toLowerCase() == x) strong -= 2;
        else { strong += 2; }
        String[] mas = {"!","@","#","$","%","^","&","*","(",")","-","=","+","<",">","/",",",".","`"};
        for (int i= 0; i < x.length();i++){
            if (x.contains(mas[i])){
                strong += 11;
            }
        }
        if(strong == 0 || strong == -1){
            strong = 1;
        }
        if (strong >= 10){
            strong = 10;
        }
        return strong;
    }
}
